package clusterServer;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;


public class HeartBeatServerUDP extends Thread {
	private String ipAddress;
	private int portNumber;
	HeartBeatServerUDP(int portNumber, String ipAddress){
		this.ipAddress = ipAddress; 
		this.portNumber = portNumber;
	}

	@Override
	public void run() {
		try {
			startSendPckages();
		} catch (Exception e) {
			ConsoleColors.print(ConsoleColors.RED_BOLD,"Fail when try to launch UPD Server.");
			e.printStackTrace();
		}
	}

	public static long incrementSec(long sec) {
		return sec + 5000;
	}

	@SuppressWarnings("static-access")
	public void startSendPckages()throws Exception {
		ConsoleColors.print(ConsoleColors.BLUE_BOLD, "[INFO] UDP Server Started.");
		InetAddress group = InetAddress.getByName("230.0.0.1"); 
		int port = 4446; 
		byte[] buf = null; 

		DatagramPacket packet = null; 
		DatagramSocket socket = new DatagramSocket(); 

		try {   
			while(true) {
				currentThread().sleep(Constants.PINGINTERVALTIME);
				
				//send ip and TCP open port
				String messageForClient = ipAddress + " " + portNumber; 	
				buf = messageForClient.getBytes(); 
				packet = new DatagramPacket(buf, buf.length, group, port); 
				socket.send(packet);	

			}
		} finally { 
			socket.close(); 
		}
	}
}
