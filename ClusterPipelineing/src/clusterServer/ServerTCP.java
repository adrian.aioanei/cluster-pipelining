package clusterServer;

import java.io.*;
import java.net.*;

public class ServerTCP extends Thread {
	private ServerSocket listener = null;

	@Override
	public void run() {
		try {
			runTCPServer();
		} catch (Exception e) {
			System.out.println("[FAIL] when try to start TCP Server");
			e.printStackTrace();
		}
	}

	public static PackageContent parseReceivedMessage(String msg) throws IOException {
		PackageContent pack =new PackageContent();
		String[] recvData = msg.split(" ");

		pack.setMessageForCurrentClient(recvData[0]);
		pack.setReplicationLevel(Integer.parseInt(recvData[1]));
		if(pack.getReplicationLevel() > 1) {
			//we hove other clients
			pack.setIpAddress(recvData[2]);
			pack.setPortNumber(Integer.parseInt(recvData[3]));
			pack.setReplicationLevel(pack.getReplicationLevel() - 1); //decrese replicationLevel

			//data to be send to the next client
			String socketDetails = new String();
			for(int i = 4;i<recvData.length;i++) {
				socketDetails += recvData[i];
				socketDetails += " ";
			}
			pack.setContentForNextClient(socketDetails);
		}else {
			System.out.println(ConsoleColors.RED_BRIGHT + "Package will not be send further.We don't have any other available sockets.\n" + ConsoleColors.RESET);
		}
		return pack;
	}

	public final static void clearConsole()
	{
		//Clears Screen in java
		try {
			if (System.getProperty("os.name").contains("Windows"))
				new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
			else
				Runtime.getRuntime().exec("clear");
		} catch (IOException | InterruptedException ex) {}
	}

	public void writeMessageToConsole(PackageContent pack,Socket connectionSocket) throws UnknownHostException {
		
		System.out.println("\n********************************************************");
		System.out.println(ConsoleColors.RED_BOLD + "Package details" + ConsoleColors.RESET);
		System.out.println("********************************************************");
		System.out.println(ConsoleColors.BLUE_BOLD + "\tI am :" + this.getServerAddress() + ":" + this.getCurrentTCPServerPort());
		System.out.println(ConsoleColors.BLUE_BOLD + "\tI received package from :" + connectionSocket.getInetAddress() + ":"+connectionSocket.getPort() + ConsoleColors.RESET);
		System.out.println("********************************************************");

		System.out.println(ConsoleColors.RED_BOLD + "Package content" + ConsoleColors.RESET);
		System.out.println("********************************************************");
		System.out.println(pack.toString());

		System.out.println("********************************************************");
		System.out.println(ConsoleColors.RED_BOLD + "Next Package hop" + ConsoleColors.RESET);
		System.out.println("********************************************************");
	}

	public void runTCPServer()  throws Exception{
		String clientMessage;

		try {
			ConsoleColors.print(ConsoleColors.BLUE_BOLD, "[INFO] TCP Server Started.");
			listener = new ServerSocket(0);

			while(true){
				Socket connectionSocket = listener.accept();
				BufferedReader clientData = 
						new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
				clientMessage = clientData.readLine();

				PackageContent pack = parseReceivedMessage(clientMessage);

				if(null != pack) {	
					clearConsole();
					writeMessageToConsole(pack,connectionSocket);

					String messageForNextClient = pack.getMessageForCurrentClient() + " " + 
							pack.getReplicationLevel() + " " + 
							pack.getContentForNextClient();

					if(pack.getIpAddress() != null)
					{
						ClientTCP sendForward = new ClientTCP(pack.getPortNumber(),pack.getIpAddress(),messageForNextClient);
						sendForward.start();
					}

				}else
					System.out.println("[STOP] I don't have other socket to send fourther.");
			}
		}

		finally {
			if(null != listener)
				try {
					listener.close();
				}catch(IOException e) {
					System.out.println("Exeption throw when trying to close server socket: ");
					e.printStackTrace();
				}
		}
	}

	public int getCurrentTCPServerPort() { 
		Error er= Error.InvalidPort;
		if(null == listener)
			return er.ordinal();
		else return listener.getLocalPort();
	}

	public String getServerAddress() throws UnknownHostException {
		String[] data = null;
		try {
			String status = InetAddress.getLocalHost().toString();
			data = status.split("/");
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}		
		return data[1];
	}
}
