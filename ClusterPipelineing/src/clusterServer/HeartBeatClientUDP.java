package clusterServer;

import java.io.IOException;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class HeartBeatClientUDP extends Thread {
	private static Map<String, Long> ipTable= null;
	HeartBeatClientUDP(){
		ipTable = new ConcurrentHashMap<>();
	}

	@Override
	public void run() {
		startReicevePackages();
	}

	public void insertData(DatagramPacket packet) {
		int size = packet.getLength();
		byte[] packageContent = packet.getData();
		byte[] releventData = new byte[size];

		for(int i = 0;i<size;i++)
			releventData[i] = packageContent[i];

		String strPackageContent = new String(releventData);	

		if(!ipTable.containsKey(strPackageContent))
			ipTable.put(strPackageContent ,System.currentTimeMillis());
		else 
			ipTable.put(strPackageContent, System.currentTimeMillis());

		removeStoppedMachines();
	}

	public void removeStoppedMachines() {
		for(Map.Entry<String, Long> entry: ipTable.entrySet()) 
			if(System.currentTimeMillis() - entry.getValue() > Constants.REMOVEINTERVALTIME) 
				ipTable.remove(entry.getKey());
	}

	//be sure I am not sending the package to myself
	public static LinkedList<String> getTargetMachine(String socketAddress, int replicationLevel) {
		int numberOfSockets = 0;
		LinkedList<String> targetSockets = new LinkedList<String>();
		for(Map.Entry<String, Long> entry: ipTable.entrySet()) {
			if(entry.getKey().equals(socketAddress))
				continue;
			else {
				if(numberOfSockets < replicationLevel)
					targetSockets.add(entry.getKey());
				numberOfSockets++;
			}
		}
		return targetSockets;
	}

	public void startReicevePackages() {
		ConsoleColors.print(ConsoleColors.BLUE_BOLD, "[INFO] UDP Client Started.");
		MulticastSocket socket = null; 
		InetAddress group = null;
		byte[] buf = null; 
		int port = 4446; 

		try { 
			group = InetAddress.getByName("230.0.0.1");
			socket = new MulticastSocket(port); 
			socket.joinGroup(group); 

			//wait for packages
			while(true) {
				buf = new byte[1024]; 
				DatagramPacket packet = new DatagramPacket(buf, buf.length); 
				socket.receive(packet); 
				insertData(packet);
			}
		}catch(IOException e) {
			ConsoleColors.print(ConsoleColors.RED_BOLD, "[Fail - ClientTCP:startReicevePackages() ].");
			e.printStackTrace();
		}
		finally { 
			try {
				socket.leaveGroup(group); 
				socket.close(); 
			}catch(IOException e) {
				ConsoleColors.print(ConsoleColors.RED_BOLD, "[Fail - ClientTCP:startReicevePackages() ] when trying to close the socket.");
				e.printStackTrace();
			}
		}
	}

	public static Map<String, Long> getAvailableMachines(){ return ipTable;}

	public static int getNumberOfClients() {return ipTable.size();}

	public static void printIpTabeles() {
		for(Map.Entry<String, Long> entry: ipTable.entrySet()) {
			System.out.println(entry.getKey());
		}
	}
}
