package clusterServer;

public enum Error {
    InvalidServerSocket,
    InvalidPort,
    InvalidIPAddress
}
