package clusterServer;

public class Constants {
	public static final int PINGINTERVALTIME = 3000;//send UDP package from 3 to 3 sec
	public static final int REMOVEINTERVALTIME = 4000; // remove a socket from map if the diff from the old date and current date is bigger then 4s
}
