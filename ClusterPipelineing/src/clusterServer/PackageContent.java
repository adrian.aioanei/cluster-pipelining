package clusterServer;

public class PackageContent {
	private int     portNumber;
	private int     replicationLevel;
	private String  ipAddress;
	private String  contentForNextClient;
	private String  messageForCurrentClient;

	PackageContent(){}

	PackageContent( int portNumber ,int replicationLevel,
			String ipAddress,
			String contentForNextClient)
	{
		this.portNumber = portNumber;
		this.replicationLevel = replicationLevel;
		this.ipAddress = ipAddress;
		this.contentForNextClient = contentForNextClient;
	}

	public int getPortNumber() {
		return portNumber;
	}

	public String getMessageForCurrentClient() {
		if(null == messageForCurrentClient)
			return null;
		else
			return messageForCurrentClient;
	}

	public void setMessageForCurrentClient(String messageForCurrentClient) {
		this.messageForCurrentClient = messageForCurrentClient;
	}

	public void setPortNumber(int portNumber) {
		this.portNumber = portNumber;
	}

	public int getReplicationLevel() {
		return replicationLevel;
	}

	public void setReplicationLevel(int replicationLevel) {
		this.replicationLevel = replicationLevel;
	}

	public String getIpAddress() {
		if(null == ipAddress)
			return null;
		else
			return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getContentForNextClient() {
		if(null == contentForNextClient)
			return null;
		else
			return contentForNextClient;
	}

	public void setContentForNextClient(String contentForNextClient) {
		this.contentForNextClient = contentForNextClient;
	}

	public String isValid(String s) {
		if(null == s)
			return null;
		else
			return s;
	}

	@Override
	public String toString() {
		String out = ConsoleColors.BLUE_BOLD + "\tMessage for current client : " + isValid(this.messageForCurrentClient) +"\n"
				+ "\tIpAddress of the next client : " + isValid(this.ipAddress) + "\n"
				+ "\tPort of the next client : " + this.portNumber + "\n"
				+ "\tReplication Level : " + this.replicationLevel + "\n"
				+ "\tContent for the next client : " + isValid(this.contentForNextClient)+ "\n" + ConsoleColors.RESET;

		return out;
	}

	public boolean isValid() {
		return !this.ipAddress.isEmpty();
	}

}
