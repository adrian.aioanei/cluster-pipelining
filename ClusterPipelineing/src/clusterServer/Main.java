package clusterServer;

import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;

public class Main {
	static Scanner keyboard = new Scanner(System.in);
	public static boolean putMessage() throws InterruptedException {
		Thread.sleep(100);
		ConsoleColors.print(ConsoleColors.GREEN_BOLD, "Do you want to send a message to your colleagues (y/n) ?");
		try {
			String userInput = keyboard.nextLine();
			if( userInput.equals("y") || userInput.equals("yes")) 
				return true;

			else if(userInput.equals("n") || userInput.equals("no"))
				return false;
			else {
				ConsoleColors.print(ConsoleColors.RED_BOLD_BRIGHT, "Invalid option. Try again with a new instance!");
				System.exit(-1);
			}
		}catch(NumberFormatException ex) {
			ConsoleColors.print(ConsoleColors.RED_BOLD, "Your input is not a valid string.");
			System.exit(-1);
		}
		return false;
	}

	public static int getReplicationLevel() {
		int userInput = 0;
		try {
			ConsoleColors.print(ConsoleColors.GREEN_BOLD, "How many colleagues do you want to spam ?");
			userInput = Integer.parseInt(keyboard.nextLine());
		}catch(NumberFormatException ex) {
			ConsoleColors.print(ConsoleColors.RED_BOLD, "Your input is not a valid integer.");
			System.exit(-1);
		}
		return userInput;
	}

	public static String getMessage() {
		String userInput = "";
		try {
			ConsoleColors.print(ConsoleColors.GREEN_BOLD, "Insert message you want to send : ");
			userInput = keyboard.nextLine();
		}catch(NumberFormatException ex) {
			ConsoleColors.print(ConsoleColors.RED_BOLD, "Your input is not a valid string.");
			System.exit(-1);
		}
		return userInput;
	}

	public static String[] splitPortAndIp(String socketName) {
		String[] data = socketName.split(" ");
		return data;
	}

	public static String convertListToString(LinkedList<String> targetSockets) {
		StringBuilder string = new StringBuilder();
		Iterator<?> it = targetSockets.descendingIterator();

		while (it.hasNext()) {
			string.append(it.next());
			string.append(" ");
		}
		return string.toString();
	}

	public static boolean reIterate() {
		System.out.println("Do you want to send another message? (y - yes, n - exit process)");
		String userInput = keyboard.nextLine();
		if( userInput.equals("y") || userInput.equals("yes")) 
			return true;
		else {
			ConsoleColors.print(ConsoleColors.BLUE_BOLD, "[INFO] A new node has been added to the cluster. Right now there are " + HeartBeatClientUDP.getNumberOfClients() + "collegs.");
			return false;
		}
	}

	public static String getPort(String[] socket) {return socket[1];}

	public static String getIpAddress(String[] socket) {return socket[0];}

	public static void runClient(ServerTCP TCPserver) throws UnknownHostException {		
		String message = new String();
		int replicationLevel = 0;

		message = getMessage();
		replicationLevel = getReplicationLevel();
		int k = HeartBeatClientUDP.getNumberOfClients();

		if(replicationLevel >= k || replicationLevel <= 0) {
			String content = "You want to send messages to " + replicationLevel + " people but you have only " + (HeartBeatClientUDP.getNumberOfClients() -1) + " available collegs."; 
			ConsoleColors.print(ConsoleColors.RED_BOLD_BRIGHT, content);
			System.exit(-1);
		}
		else {
			String contentToSend = message + " " + (replicationLevel) + " ";

			String localSocketAddress         = TCPserver.getServerAddress()+ " " + TCPserver.getCurrentTCPServerPort();
			LinkedList<String> targetSockets  = HeartBeatClientUDP.getTargetMachine(localSocketAddress,replicationLevel);

			String[] socketData = splitPortAndIp(targetSockets.poll());
			String targetIp     = getIpAddress(socketData);
			String targetPort   = getPort(socketData);

			contentToSend      += convertListToString(targetSockets);
			ClientTCP TCPclient = new ClientTCP(Integer.parseInt(targetPort),targetIp,contentToSend);
			TCPclient.start();
		}
	}

	public static void main(String args[])  {	
		try {
			ServerTCP TCPserver = new ServerTCP();
			TCPserver.start();
			Thread.sleep(100);

			HeartBeatServerUDP heartBeatServerUDP = new HeartBeatServerUDP(TCPserver.getCurrentTCPServerPort(), TCPserver.getServerAddress());
			HeartBeatClientUDP heartBeatClientUDP = new HeartBeatClientUDP();

			heartBeatServerUDP.start();
			heartBeatClientUDP.start();

			boolean flag = true;
			do {
				if(putMessage()) {
					runClient(TCPserver);
					flag = reIterate();
				}else {
					flag = false;
					ConsoleColors.print(ConsoleColors.BLUE_BOLD, "[INFO] A new node has been added to the cluster. Right now there are " + HeartBeatClientUDP.getNumberOfClients() + " collegs.");
				}
			}while(flag);

		}catch(IndexOutOfBoundsException e) {
			ConsoleColors.print(ConsoleColors.RED_BOLD, "[FAIL] in main. Index out of bound in main.");
			e.getStackTrace();
		}catch(InterruptedException e) {
			ConsoleColors.print(ConsoleColors.RED_BOLD, "[FAIL] in main, Thread problems.");
			e.printStackTrace();
		}catch(UnknownHostException e) {
			ConsoleColors.print(ConsoleColors.RED_BOLD, "[FAIL] in main. Unknown host.");
			e.printStackTrace();
		}catch(Exception e){
			ConsoleColors.print(ConsoleColors.RED_BOLD, "[FAIL] in main.");
			e.printStackTrace();
		}  
	}

}
