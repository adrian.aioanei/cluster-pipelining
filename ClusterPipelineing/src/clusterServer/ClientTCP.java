package clusterServer;

import java.io.*;
import java.net.*;

public class ClientTCP extends Thread {

	private String ipAddress;
	private String messageToSend;
	private int portNumber;


	ClientTCP(int portNumber, String ipAddress, String messageToSend){
		this.ipAddress = ipAddress;
		this.portNumber = portNumber;
		this.messageToSend = messageToSend;

		String message = "\tStart new TCP client for : " + this.ipAddress+ ":" +this.portNumber;
		ConsoleColors.print(ConsoleColors.BLUE_BOLD, message);
		
		message = "\tConnecting to " + this.ipAddress + ":" + this.portNumber + ConsoleColors.RESET + "\n\n";
		ConsoleColors.print(ConsoleColors.BLUE_BOLD, message);
	}
	@Override
	public void run() {
		try {
			sendMessageUsingTCP(this.messageToSend);
		} catch (Exception e) {
			ConsoleColors.print(ConsoleColors.RED_BOLD, "[Fail - ClientTCP:run() ] when trying to start TCP Client");
			e.printStackTrace();
		}
	}

	public void sendMessageUsingTCP(String message)  {
		try {
			Socket clientSocket = new Socket(ipAddress,portNumber);					
			DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
			outToServer.writeBytes(message + '\n');
			clientSocket.close();
		}catch(Exception e) {
			ConsoleColors.print(ConsoleColors.RED_BOLD, "[Fail - ClientTCP:sendMessageUsingTCP(String message) ]");
			e.printStackTrace();
		}
	}
}
