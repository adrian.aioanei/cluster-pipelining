# Cluster-Pipelining

Implement a distributed application for data replication inside a cluster by using the pipelining technique.
The communication inside the cluster for data replication will use `TCP/IP` connections.
Develop a client application that will be able to connect to any cluster node in order to store a data set, at the cluster level, with
a `unique id` and a replication factor specified at runtime (example: `put(long uid, byte[] data, int replicationLevel)`.


# Example of output
![Example of run for 4 clients](https://i.imgur.com/VEQr4d0.png)
